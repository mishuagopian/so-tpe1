#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "getnum.h"
#include "defs.h"

#include <errno.h>

static key_t keyout=0xBEEF1;
static command clt_commands[CLT_CMD] = {{"verCartelera", verCartelera}, {"verAsientosDisponibles", verAsientosDisponibles}, 										{"reservarAsientos", reservarAsientos}, {"verReserva", 										verReserva},{"confirmarReserva", confirmarReserva},
									{"cancelarReserva", cancelarReserva}};

int sockid;
int logueado=0;
reserva res;

void verCartelera(){
	char c=' ';
	printf("\n");
	write(sockid,"1",1);
	while( c!='\0'){
		read(sockid,&c,1);
		if(c!='\0' && (isalpha(c)||c==' '||c=='\n'||isdigit(c))){
			printf("%c",c);
		}	
	}
}


void verAsientosDisponibles(){
	char codigo[5];
	char c;
	printf("\nIngrese el codigo de la pelicula\n");
	scanf("%4s",codigo);
	write(sockid,"2",1);
	write(sockid,codigo,4);
	read(sockid,&c,1);
	if(c=='0'){ 
		printf("Codigo de funcion invalido\n");
		return;
	}	
	printf("\nAsientos disponibles:\n(0: disponible - X: ocupado)\n\n");
	while(c!='\0'){	
		read(sockid,&c,1);
		printf("%c",c);
	
	}
	printf("\np a n t a l l a \n");
}

void reservarAsientos(){
	char c;
	int i;
	write(sockid,"3",1);
	if(res.usada!=FALSE){
		printf("\nYa tiene una reserva actualmente, solo puede tener una activa\n");	
		return;		
	}
	printf("\nIngrese el codigo de la pelicula para la cual desea reservar asientos\n");
	scanf("%4s",res.codigoFuncion);
	do{
		res.cantAsientos=getint("Elija la cantidad de asientos a reservar:(max %d)\n",MAX_SIT);
	}while(res.cantAsientos>MAX_SIT || res.cantAsientos<1);
	for(i=0;i<res.cantAsientos;i++){
		res.asientos[i]=100*getint("asiento %d: \n-elija la fila:  ",i+1);
		res.asientos[i]+=getint("-elija la columna:  ");
	}
	write(sockid,res.codigoFuncion,4);
	read(sockid,&c,1);
	if(c!='1'){
		printf("\nLa funcion no existe\n");
		return;
	}
	c=res.cantAsientos;
	write(sockid,&c,1);
	for(i=0;i<res.cantAsientos;i++){
		c=res.asientos[i]/100;
		write(sockid,&c,1);
		c=res.asientos[i]%100;
		write(sockid,&c,1);
	}
	read(sockid,&c,1);
	if(c=='1'){
		res.usada=TRUE;
		printf("\nReserva realizada con exito\n");	
	}
	else{
		printf("\nAlguno de los asientos elegidos esta ocupado o no es valido\n");	
	}
}

void verReserva(){
	int i;
	if(res.usada!=FALSE){
		printf("\nSu reserva es para la funcion de codigo %s\n",res.codigoFuncion);
		printf("y ha reservado %d asientos.\n",res.cantAsientos);
		printf("Los mismos son:\n");
		for(i=0;i<res.cantAsientos;i++){
			printf("asiento: %d,  fila: %d, columna: %d.\n", i+1, res.asientos[i]/100,res.asientos[i]%100);
		}
	}else{
		printf("\nUsted no tiene reserva.\n");
	}
}

void confirmarReserva(){
	if(res.usada==FALSE){
		printf("\nUsted no tiene reserva.\n");
	}
	else{
		printf("\nSu reserva ha sido confirmada con exito\n");	
		res.usada=FALSE;
	}	
	return;
}

void cancelarReserva(){
	char c;
	int i;
	if(res.usada==FALSE){
		printf("\nUsted no tiene reserva\n");
		return;
	}
	write(sockid,"5",1);
	write(sockid,res.codigoFuncion,4);
	c=res.cantAsientos;
	write(sockid,&c,1);
	for(i=0;i<res.cantAsientos;i++){
		c=res.asientos[i]/100;
		write(sockid,&c,1);
		c=res.asientos[i]%100;
		write(sockid,&c,1);
	}
	read(sockid,&c,1);
	if(c=='1'){
		printf("\nReserva cancelada con exito\n");
	}
	else{
		printf("\nLa funcion de su reserva ha sido cancelada\n");
	}
	res.usada=FALSE;
}

void login(){
	logueado=1;
}

int main(int argc, char *argv[]){
	
	int c;
	int i;
	char aux[25];
	struct sockaddr_in name;
	struct in_addr addr;
	sockid=socket(AF_INET,SOCK_STREAM,0);
	if(sockid==-1){
		printf("Error Fatal sockid\n");
		return -1;
	}
	name.sin_family=AF_INET;
	if(inet_pton(AF_INET,argv[1],&addr.s_addr)!=1){
		printf("Error Fatal pton\n");
		return -1;
	}
	name.sin_addr=addr;
	name.sin_port=htons(5001);
	if(connect(sockid,(struct sockaddr*)&name,sizeof(name))==-1){
		printf("Error Fatal connect\n");
		return -1;
	}
	res.usada=FALSE;
	printf("Bienvenido\n");
	while(TRUE){
		printf("\nIngrese su comando : \n- ");
		scanf("%s",aux);
		for(i=0;i<CLT_CMD;i++){
			if(!strcmp(aux,clt_commands[i].name)){
				clt_commands[i].function();
				break;
			}
		}
		if(i==CLT_CMD){
			printf("\nComando invalido\n");
		}
	}
	return 0;
}


